st - simple terminal
--------------------
st is a simple terminal emulator for X which sucks less.


Requirements
------------
In order to build st you need the Xlib header files.


Installation
------------
Edit config.mk to match your local setup (st is installed into
the /usr/local namespace by default).

Afterwards enter the following command to build and install st (if
necessary as root):
```sh
make clean install
```

Running st
----------
If you did not install st with make clean install, you must compile
the st terminfo entry with the following command:
```sh
tic -sx st.info
```
See the man page for additional details.

Modifications
-------------
This version of st has the following patches:
- [[http://st.suckless.org/patches/xresources/][xrdb]]
- [[http://st.suckless.org/patches/scrollback/][scrollback]]

I also "fixed" the delete key with:
```c
{ XK_Delete, XK_ANY_MOD, "\033[C\177", 0, 0},
{ XK_Delete, XK_NO_MOD,  "\033[C\177", 0, 0},
```
in the config.h file. (To clarify, it deletes one character after the
cursor).

Credits
-------
Based on Aurélien APTEL <aurelien dot aptel at gmail dot com> bt source code.

